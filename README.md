
# Object Calisthenics

This repository is an implementation of the 9 Object Calisthenics rules born to give some guidelines for writing
better code in object-oriented programming languages.

| Rule                                                                                                                   | Pmd        | Checkstyle  | Note |
| ---------------------------------------------------------------------------------------------------------------------- | :--------: | :----------:|:---: |
| [Rule 1](https://williamdurand.fr/2013/06/03/object-calisthenics/#1-only-one-level-of-indentation-per-method)          |     ×     |     ×       | Con PMD bisognerebbe dire che non ci può essere uno Statement "puro" dentro un altro Statement ma per XPath tutti i nodi sono degli Statement.|
| [Rule 2](https://williamdurand.fr/2013/06/03/object-calisthenics/#2-dont-use-the-else-keyword)                         |     V      |     ×       | |
| [Rule 3](https://williamdurand.fr/2013/06/03/object-calisthenics/#3-wrap-all-primitives-and-strings)                   |     V      |     ×       | |
| [Rule 4](https://williamdurand.fr/2013/06/03/object-calisthenics/#4-first-class-collections)                           |     ×      |     ×       | Non è possibile avere degli if nelle espressioni XPATH. Altrimenti: se FieldDeclaration contiene //ReferenceType//ClassOrInterfaceType[@Image= "ArrayList"] restituisci nodi che contengono altre definizioni. |
| [Rule 5](https://williamdurand.fr/2013/06/03/object-calisthenics/#5-one-dot-per-line)                                  |     ×      |     ×       | |
| [Rule 6](https://williamdurand.fr/2013/06/03/object-calisthenics/#6-dont-abbreviate)                                   |     ×      |     ×       | Cosa significa? |
| [Rule 7](https://williamdurand.fr/2013/06/03/object-calisthenics/#7-keep-all-entities-small)                           |     ×      |     ×       | |
| [Rule 8](https://williamdurand.fr/2013/06/03/object-calisthenics/#8-no-classes-with-more-than-two-instance-variables)  |     V      |     ×       | Parzialmente giusto ma non rileva più dichiarazioni nella stessa riga le conta come una sola dichiarazione.|
| [Rule 9](https://williamdurand.fr/2013/06/03/object-calisthenics/#9-no-getterssettersproperties)                       |     ×      |     ×       | |

Legend:

- ✓ => Implemented
- × => Not implemented

Note Installazione

- Installare PDM (https://pmd.github.io/latest/pmd_userdocs_installation.html)
- Installare JAVAFX 11 (https://gluonhq.com/products/javafx/)
- Aggiugere al file .zshrc (oppure .bash) le seguenti cose:
    alias pmd="/Applications/pmd-bin-6.34.0/bin/run.sh designer"
    export JAVAFX_HOME="/Applications/javafx-sdk-11.0.2/"
- Lanciare digitando "pmd"

